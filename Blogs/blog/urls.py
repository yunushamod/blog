from django.urls import path
from . import views
from .feeds import LatestPostsFeed
urlpatterns = [
    path('feed/', LatestPostsFeed(), name='feed'),
    path('', views.list, name='list'),
    path('<slug:tag_slug>/', views.list, name='tag'),
    path('<int:id>/share/', views.share, name='share'),
    path('<int:year>/<int:month>/<int:day>/<slug:slug>/', views.detail, name='detail'),
    
]