from django.shortcuts import render, get_object_or_404
from .models import Post, Tag, Comment
from .forms import CommentForm, EmailShareForm
from django.core.mail import send_mail
# Create your views here.

def list(request, tag_slug=None):
    posts = Post.objects.filter(status='published')
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        posts = tag.posts.all()
    return render(request, 'blog/list.html', {'posts':posts})
def detail(request, year, month, day, slug):
    post = get_object_or_404(Post, publish__year=year,publish__month=month, 
    publish__day=day, slug=slug, status='published')
    comments = post.comments.filter(active=True)
    new_comment = None
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment_form = comment_form.save(commit=False)
            comment_form.post = post
            comment_form.save()
            new_comment = comment_form
    else:
        comment_form = CommentForm()

    return render(request, 'blog/detail.html', {'post':post,'comments':comments, 
    'comment_form':comment_form,'new_comment': new_comment})

def share(request, id):
    post = get_object_or_404(Post, id=id, status='published')
    sent = False
    if request.method == 'POST':
        share_form = EmailShareForm(request.POST)
        if share_form.is_valid():
            cd = share_form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())
            subject = f"{cd['name']} recommends you read {post.title}"
            message = f"Read {post.title} at {post_url}\n\n {cd['name']}'s comments: {cd['comments']}"
            send_mail(subject,message, 'admin@blog.com', [cd['to']])
            sent=True

    else:
        share_form = EmailShareForm()
    return render(request, 'blog/share.html', {'post':post, 'form':share_form, 'sent': sent})



