from django.contrib import admin
from .models import Post, Tag, Comment
# Register your models here.

"""
class TagInline(admin.StackedInline):
    model = Tag
    can_delete = False
    verbose_name_plural = 'tags'
"""

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'publish', 'created', 'status']
    date_hierarchy ='publish'
    prepopulated_fields = {'slug':('title',)}
    list_filter = ['created', 'publish', 'status']
    search_fields = ('title',)
    filter_horizontal = ('tags',)

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['post','author', 'email', 'created', 'active']
    date_hierarchy = 'created'
    list_filter = ['post', 'created', 'updated', 'active']
    search_fields = ('post', 'author')

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['title','slug']
    prepopulated_fields = {'slug': ('title', )}
    search_fields = ('title',)
    filter_horizontal = ('posts',)
    fields = ['title', 'slug', 'posts']


