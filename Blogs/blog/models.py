from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse

# Create your models here.

STATUS = (
    ('draft', 'Draft'),
    ('published', 'Published'),
)

class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')

class Post(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20, choices=STATUS, default='draft')
    objects = models.Manager()
    published = PublishedManager()
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('detail', args=[self.publish.year, self.publish.month, self.publish.day, self.slug])
    class Meta:
        ordering: ['-publish']

class Tag(models.Model):
    title = models.CharField(max_length=100)
    posts = models.ManyToManyField(Post, related_name='tags')
    slug = models.SlugField(unique=True)
    def __str__(self):
        return self.title

class Comment(models.Model):
    author = models.CharField(max_length=250)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    email = models.EmailField(blank=True)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    def __str__(self):
        return f"{self.author}'s comment on {self.post}"
    class Meta:
        ordering = ['-created']





